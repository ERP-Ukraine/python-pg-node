FROM python:2.7.13
MAINTAINER ERP Ukraine <info@erp.co.ua>

# Install some deps
RUN apt-get update && apt-get install -y \
    apt-utils \
    libsqlite3-dev \
    locales \
    nodejs \
    sqlite3

RUN locale-gen en_US.UTF-8 && \
    localedef -i en_US -f UTF-8 en_US.UTF-8

RUN apt-get install -y \
    postgresql-9.4 \
    postgresql-client-9.4 \
    sudo

RUN service postgresql start && \
    pg_dropcluster --stop 9.4 main && \
    pg_createcluster --locale en_US.UTF-8 --start 9.4 main && \
    sudo -u postgres sh -c 'createuser -s root & createdb root'

RUN apt-get install -y \
    curl \
    expect-dev \
    python-lxml \
    python-paramiko \
    python-redis \
    python-simplejson \
    python-serial \
    python-yaml

RUN curl -sL https://deb.nodesource.com/setup_4.x | bash -

RUN apt-get update && apt-get install -y \
    nodejs \
    node-less

RUN pip install -q \
    ofxparse \
    unidecode \
    unicodecsv \
    lxml \
    xlsxwriter

#RUN git clone --depth=1 https://github.com/ERP-Ukraine/maintainer-quality-tools.git /root/maintainer-quality-tools
#ENV ODOO_REPO="odoo/odoo"
#ENV VERSION="10.0"
#ENV TRANSIFEX="0"
#ENV TRANSIFEX_USER='transbot@odoo-community.org'
#ENV PATH=/root/maintainer-quality-tools/travis:${PATH}
#ENV LINT_CHECK="1"
#ENV TESTS="0"
#RUN travis_install_nightly
#ENV LINT_CHECK="0"
#ENV TESTS="1"
#ENV TRAVIS_BUILD_DIR=./
#RUN travis_install_nightly
